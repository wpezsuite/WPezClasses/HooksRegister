## WPezClasses: Hooks Register

_When you register your WordPress hooks The ezWay, those arrays are easier to configure. They (i.e., the arrays) can also be filtered, and manipulated in ways that (hard) coding you hooks can't be.__

Save yourself from the tyranny of WordPress tradition and stop hardcoding your hooks. Please.   

> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Simple Example

This is a simple work in progress example. 

https://gitlab.com/WPezPlugins/wpez-osm/blob/master/App/ClassPlugin.php

The general idea is to separate the what (i.e., the class doing the work) from the when (i.e., the action or filter). Then take it a step further and configure (arrays of) those hooks instead of hardcoding them. Simple, clean and naturally ez.

Note: If you take a quick look at the method setPropertyDefaults() you'll notice there's actually an active key. As is common to The ezWay, this is simple on / off bool flag to make manipulation ez'ier.  



### FAQ

__1) Why?__

Hardcoded hooks (i.e., add_action()s and add_filter()s) are a PITA. As arrays they become ez'ier and more natural for others to manipulate. Arrays are also ez'er to maintain. 


__2) Can I use this in my plugin or theme?__

Yes, but to be safe, please change the namespace. 


 __3) - I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 



### HELPFUL LINKS

None at this time

### TODO

- Provide a better example(s)



### CHANGE LOG


- v0.5.1 - 15 Aprl 2019
  - Updated: Namespace
  - updated: README 
  
- v0.5.0 - 13 Feb 2019
  - This has been in development for quite some time, but not independently repo'ed until now. Please pardon the delay. 